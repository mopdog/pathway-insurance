<?php
get_header(); ?>

<div class="content">
<div class="units-container">
	<div class="units-row">
		<div class="unit-100 content-body">
			<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<p><?php the_content(); ?></p>
			<?php endwhile; ?>
			<?php else : ?>
		<?php endif; ?>
		</div>
	</div>
</div>
</div>
<?php get_footer(); ?>
