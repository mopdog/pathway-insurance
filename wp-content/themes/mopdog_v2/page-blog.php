<?php
/*Template Name: Blog Posts*/
get_header(); ?>

<div class="units-container">
	<div class="units-row">
		<div class="unit-100 blog content-body">
			<p>Our team of industry experts provides a wealth of knowledge about commercial and 
			personal insurance. Read our Pathway Insurance Group blog to gain even more insight 
			about what options will work best for you, your family and your business.</p>
			<?php 
			// the query
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array(
				'posts_per_page' => 5,
				'paged' => $paged,
				'post_type' => $post);

			$the_query = new WP_Query( $args ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

			  <!-- pagination here -->

			  <!-- the loop -->
			  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			    <h6><?php the_time('F j, Y'); ?></h6>
			    <?php the_excerpt(); ?>
			    <a class="read-more" href="<?php the_permalink(); ?>">read more</a>
			    <hr/>
			  <?php endwhile; ?>
			  <!-- end of the loop -->

			  <!-- pagination here -->
<nav>
    <ul>
        <li><?php previous_posts_link( '&laquo; PREV', $the_query->max_num_pages) ?></li> 
        <li><?php next_posts_link( 'NEXT &raquo;', $the_query->max_num_pages) ?></li>
    </ul>
</nav>
 

			  <?php wp_reset_postdata(); ?>
		
			<?php else:  ?>
			  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
