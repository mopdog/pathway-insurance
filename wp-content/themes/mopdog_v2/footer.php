<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package mopdog
 */
?>
</div> <!-- End page -->
	<footer id="footer">
	<div class="units-container">
	<div class="units-row">
		<ul class="blocks-2">
			

			<li>
				<h5>OUR SERVICES</h5>
				<ul class="footer-menu">
					<li><a href="<?php bloginfo('url'); ?>/commercial-insurance">Commercial Insurance</a></li>
					<li><a href="<?php bloginfo('url'); ?>/life-insurance">Life Insurance</a></li>
					<li><a href="<?php bloginfo('url'); ?>/personal-insurance">Personal Insurance</a></li>
					<li><a href="<?php bloginfo('url'); ?>/customized-solutions">Customized Solutions</a></li>
				</ul>

				<h5>ABOUT US</h5>
				<ul class="footer-menu">
					<li><a href="<?php bloginfo('url'); ?>/blog">Blog</a></li>
					<li><a href="<?php bloginfo('url'); ?>/join-our-team">Join Our Team</a></li>
				</ul>
			</li>


			<li class="footer-contact">
				<h5>CONTACT US</h5>
				<h6>Questions? Call Us!</h6>
				
				<?php if(get_field('footer_phone_number', 'option')): ?>
					<p>
					<a href="tel: <?php the_field('footer_phone_number' , 'option'); ?> "><?php the_field('footer_phone_number', 'option'); ?></a>
					</p>
				<?php endif; ?>

				<h6>Pathway Insurance GROUP</h6>
				
				<?php if(get_field('footer_address', 'option')): ?>
					<p>
						<?php the_field('footer_address', 'option'); ?>
					</p>
				<?php endif; ?>
				
				<!--<p><a href="https://www.google.com/maps/place/3121+St+Croix+Trail+S+%23203/@44.8999881,-92.7834104,16z/data=!4m2!3m1!1s0x87f7de1c79236681:0xf9dec4c9b4954c46" target="_blank">Driving Directions</a></p>-->
			</li>
			<li class="footer-social">
				<h5>SOCIAL</h5>
				<a class="fa fa-facebook-square" href="https://www.facebook.com/pathwayinsgroup" target="_blank"></a>
				<a class="fa fa-twitter-square" href="https://twitter.com/PathwayInsgroup" target="_blank"></a>
			</li>
		</ul>
		</div>

		<div class="units-row">
			<div class="unit-100 copyright">
				<p>@ 2014 Pathway Insurance <a href="<?php bloginfo('url'); ?>/privacy-policy/">Privacy Policy</a> | 
				   <a href="<?php bloginfo('url'); ?>/terms-of-use/">Terms of Use</a> |
				   <a target="_blank" href="https://pathwayproperties.sharepoint.com/_layouts/15/start.aspx#/">Employee Resources</a>
 				</p>
			</div>
		</div>
		</div>
	</footer>


<?php wp_footer(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47692426-9', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>
