<?php
/*Template Name: Presentations and Events*/
get_header(); ?>

<div class="units-container">
	<div class="units-row">
		<div class="unit-100 blog content-body">

			<h2>Upcoming Events:</h2>
			<hr/>
			<?php
			$today = date('Ymd'); 
			// the query
			$args = array( 
				'posts_per_page' => 10,
				'post_type' => array( 'event' ),
				'meta_key' => 'date',
				'orderby' => 'meta_value_num',
				'order' => 'ASC',
				'meta_query' => array(
					array(
						'key' => 'date',
						'compare'	=> '>=',
						'value' => $today,
					)
				)

			);
			$the_query = new WP_Query( $args ); ?>
			
			<script>
				    console.log(<? echo json_encode($today); ?>);
				</script>

			<?php if ( $the_query->have_posts() ) : while( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php $postid = get_the_ID(); ?>
				<?php $flyer = get_field('flyer',$postid); ?>
				<?php $register = get_field('registration_link',$postid); ?>

				<?php $date = DateTime::createFromFormat('Ymd', get_field('date',$postid));?>
				
				<script>
				    console.log(<? echo json_encode($postid); ?>);
				</script>

				

				<div class="event">
				<h2 class="no-margin"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<h5><?php echo $date->format('F j, Y'); ?></h5>
				<p><?php the_excerpt(); ?></p>

				<?php if($flyer): ?>
				<h5><a href="<?php the_field('flyer',$postid); ?>">Download the flyer</a></h5>
				<?php else: ?>
				<h5>Flyer coming soon</h5>
				<?php endif; ?>

				<?php if($register): ?>
				<h5><a href="<?php the_field('registration_link',$postid); ?>">Registration is now available</a></h5>
				<?php else: ?>
				<h5>Registration coming soon</h5>
				<?php endif; ?>
				</div>
			
			<?php endwhile; endif; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
