<?php
/*Template Name: Home */

get_header('home'); ?>

<div class="units-container">
	<div class="units-row">
		<div class="unit-100"> <!--START COM INS-->
			<div class="units-row units-split white-block"> 
				<div class="unit-40 com-ins">
					<img src="<?php bloginfo('template_directory'); ?>/images/comicon.png">
					<h1>COMMERCIAL INSURANCE</h1>
				</div>
				<div class="unit-60 com-ins-type">
					<h4>THE strategic approach TO protecting your business</h4>
					<p>We understand you make challenging business decisions every day. 
					Choosing the right insurance provider should not be one of them. </p>

					<p>Pathway Insurance Group works with leading insurance companies and agencies 
					to package the best bundle of services. We partner with you to provide flexible, 
					competitive and cost-effective products that best fit your needs.</p>

					<p><a href="<?php bloginfo('url'); ?>/commercial-insurance">LEARN MORE</a></p>
				</div>
			</div> 
		</div> <!--END COM INS-->
		<ul class="blocks-3">
			<li class="personal home-block">
			<a href="<?php bloginfo('url'); ?>/personal-insurance">
				<div class="read-more">
					<div class="learn-line"></div>
					<h1>LEARN</h1>
					<h1>MORE</h1>
					<div class="learn-line"></div>
				</div>
				<div class="hover-color"></div>
				<h3>PERSONAL INSURANCE</h3>
				<img src="<?php bloginfo('template_directory'); ?>/images/personal.png">
				<div class="color-block">
					<p>Your family is special, so you need a specialized plan to cover 
					everything you value in life. With Pathway Insurance Group, you will feel 
					secure in knowing you are receiving the right coverage to fit your unique 
					needs and budget.</p>
				</div>
				</a>
			</li>

			<li class="customized home-block">
			<a href="<?php bloginfo('url'); ?>/customized-solutions">
				<div class="read-more">
					<div class="learn-line"></div>
					<h1>LEARN</h1>
					<h1>MORE</h1>
					<div class="learn-line"></div>
				</div>
				<div class="hover-color"></div>
				<h3>CUSTOMIZED SOLUTIONS</h3>
				<img src="<?php bloginfo('template_directory'); ?>/images/customized.png">
				<div class="color-block">
					<p>Unlike other agents, Pathway Insurance Group is not bound to any one 
					insurance company and you should not be either. We will help you take a strategic 
					approach towards reducing risk exposure and vulnerabilities, and adjust that coverage 
					as your needs change throughout the years. </p>
				</div>
				</a>
			</li>

			<li class="life home-block">
			<a href="<?php bloginfo('url'); ?>/life-insurance">
				<div class="read-more">
					<div class="learn-line"></div>
					<h1>LEARN</h1>
					<h1>MORE</h1>
					<div class="learn-line"></div>
				</div>
				<div class="hover-color"></div>
				<h3>LIFE INSURANCE</h3>
				<img src="<?php bloginfo('template_directory'); ?>/images/life.png">
				<div class="color-block">
					<p>You have spent your entire life protecting those you love. 
					Have peace of mind knowing your loved ones can still depend on you, even after 
					you are gone. Pathway Insurance Group will help you make decisions today to ensure 
					your family’s safety and security for years to come.</p>
				</div>
				</a>
			</li>
		</ul>
	</div>
</div>
<?php get_footer(); ?>
	