jQuery(function($){
	$(window).bind("load", function() {
var footer = $("#footer");
var pos = footer.position();
var height = $(window).outerHeight();
height = height - pos.top;
height = height - footer.outerHeight();
    if (height > 0) {
  footer.css({'margin-top' : height+'px'});
  }
});

	$(window).resize(function(){
		var footer = $("#footer");
		var pos= footer.position();
		var height= $(window).outerHeight();
		height = height - pos.top;
		height = height - footer.outerHeight();
		if (height > 0){
			footer.css({'margin-top' : height+'px'});
		}
	});

	    // Show color overlay when hovering over blog
    $('.home-block').hover(
    	function(){
	    	$(".hover-color",this).stop().animate({
	    		opacity: 0.60
	    	}, 300) ,
	    	$(".read-more",this).stop().fadeIn(300)  

	    } ,
	    function(){
	    	$(".hover-color",this).stop().animate({
	    		opacity: 0.0
	    	}, 300) ,
	    	$(".read-more",this).stop().fadeOut(300)  
    }).stop();


       $('.post-link').hover(
    	function(){
	    	$(".hover-color",this).stop().animate({
	    		opacity: 0.60
	    	}, 700) ,
	    	$(".read-more",this).stop().fadeIn(300)  

	    } ,
	    function(){
	    	$(".hover-color",this).stop().animate({
	    		opacity: 0.0
	    	}, 700) ,
	    	$(".read-more",this).stop().fadeOut(300)  
    }).stop();
});

// CSS3 animated & responsive dropdown menu
// http://www.red-team-design.com/css3-animated-dropdown-menu
jQuery(function($){
		/* Mobile */
$('#menu-wrap').prepend('<div id="menu-trigger">Menu</div>');
$("#menu-trigger").on("click", function(){
$("#menu").slideToggle();
});

		// iPad
		var isiPad = navigator.userAgent.match(/iPad/i) !== null;
		if (isiPad) $('#menu ul').addClass('no-transition');
});
