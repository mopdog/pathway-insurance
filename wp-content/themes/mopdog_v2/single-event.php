<?php
get_header(); ?>
	<?php $currentpage = get_the_ID(); ?>
<div class="units-container">
	<div class="units-row">

		<div class="unit-60 content-body">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php $flyerMain = get_field('flyer'); ?>
			<?php $registerMain = get_field('registration_link'); ?>
			<div id="innerbody">
			<?php if($flyerMain): ?>
				<h6><a href="<?php the_field('flyer'); ?>">Download the flyer</a></h6>
				<?php else: ?>
				<h6>Flyer coming soon</h6>
				<?php endif; ?>

				<?php if($registerMain): ?>
				<h6><a href="<?php the_field('registration_link'); ?>">Registration is now available</a></h6>
				<?php else: ?>
				<h6>Registration coming soon</h6>
				<?php endif; ?>
			<?php the_content(); ?>
			<?php endwhile; ?>
			<?php else : ?>
		<?php endif; ?>
		</div>
	</div>

	<div class="unit-40 sidebar">
			<h6><a href="<?php bloginfo('url'); ?>/presentations-events/">Upcoming Events:</a></h6>

			<?php
			$today = date('Ymd'); 
			// the query
			$args = array( 
				'posts_per_page' => 5,
				'post_type' => array( 'event' ),
				'meta_key' => 'date',
				'orderby' => 'meta_value_num',
				'order' => 'ASC',
				'meta_query' => array(
					array(
						'key' => 'date',
						'compare'	=> '>=',
						'value' => $today,
					)
				)

			);
			$the_query = new WP_Query( $args ); ?>

			<?php if ( $the_query->have_posts() ) : while( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php $postid = get_the_ID(); ?>
				<?php $flyer = get_field('flyer',$postid); ?>
				<?php $register = get_field('registration_link',$postid); ?>

				<?php $date = DateTime::createFromFormat('Ymd', get_field('date',$postid));?>
				
				<script>
				    console.log(<? echo json_encode($flyer); ?>);
				</script>

				<script>
				    console.log(<? echo json_encode($register); ?>);
				</script>
				<?php if($currentpage == $postid): ?>

				<?php else: ?>
				<div class="event">
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				<h5 class="strong"><?php echo $date->format('F j, Y'); ?></h5>

				<?php if($flyer): ?>

				<h6><a href="<?php the_field('flyer',$postid); ?>">Download the flyer</a></h6>
				<?php else: ?>
				<h6>Flyer coming soon</h6>
				<?php endif; ?>

				<?php if($register): ?>
				<h6><a href="<?php the_field('registration_link',$postid); ?>">Registration is now available</a></h6>
				<?php else: ?>
				<h6>Registration coming soon</h6>
				<?php endif; ?>
				</div>
			<?php endif; ?>

			<?php endwhile; endif; ?>
			<?php wp_reset_query(); ?>
		</div>
</div>
<?php get_footer(); ?>
