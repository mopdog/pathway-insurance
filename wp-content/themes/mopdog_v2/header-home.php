<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package mopdog
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/dist/html5shiv.js"></script>
<![endif]-->
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
			
			<header class="home">
			<div class="units-container">
				<div class="units-row">
					<div class="unit-100">
						<nav id="menu-wrap">
						<figure class="logo">
						<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" /></a>
						</figure>
						<?php wp_nav_menu( array( 
						    'theme_location' => 'primary',
						    'menu_class' => 'flexnav',
						    'items_wrap' => '<ul id="menu">%3$s</ul>', 
						    ));
						    ?>
						</nav>
					</div>
				</div>

				<div class="units-row">
					<div class="unit-50 unit-push-right">
						<h1>FOCUS ON SUCCESS.
						WE'LL COVER ALL THE REST.</h1>
						<p>Our agents have the insight, expertise and knowledge to ensure you receive 
						the best coverage for you, your family and your business. Pathway Insurance 
						Group is independent from large insurance firms, which means we offer a 
						higher value. Our agents help select the most appropriate option from 
						a wide range of choices in each area of coverage you need.</p>

						<p>We navigate the complex insurance industry for you. For more information 
						on how Pathway Insurance Group finds the coverage that best fits 
						your unique needs, please contact us at 651-207-5724.</p>
						<p><a href="<?php bloginfo('url'); ?>/about-us">LEARN MORE</a></p>
					</div>
				</div>
			</div>
				
			</header>

<div id="page" class="hfeed site"> <!-- Start Page -->
