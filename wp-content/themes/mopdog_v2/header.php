<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package mopdog
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/dist/html5shiv.js"></script>
<![endif]-->
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'thumbnail') ); ?>	
			<header class="side" <?php if(has_post_thumbnail() ): ?>style="background-image:url('<?php echo $url; ?>');" <?php endif; ?>>
			<div class="units-container">
				<div class="units-row">
					<div class="unit-100">
						<nav id="menu-wrap">
						<figure class="logo">
						<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" /></a>
						</figure>
						<?php wp_nav_menu( array( 
						    'theme_location' => 'primary',
						    'menu_class' => 'flexnav',
						    'items_wrap' => '<ul id="menu">%3$s</ul>', 
						    ));
						    ?>
						</nav>
					</div>
				</div>

				<div class="units-row">
					<div class="unit-80">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
				
			</header>

<div id="page" class="hfeed site"> <!-- Start Page -->
